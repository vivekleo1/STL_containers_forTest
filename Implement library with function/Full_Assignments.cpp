#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

struct Data{
    uint8_t x;
    double y;
   
    Data(uint8_t a, double b) {
        x = a;
        y = b;
    }
};

uint8_t x;
double y;
std::vector<Data> subSetA;
std::vector<Data> subSetB;

// threshold value of x_threshold
uint8_t x_threshold;

// input size for the container
int inputSize;

bool cmp_based_on_y(const Data& a, const Data& b)
{
    // smallest comes first
    return a.y < b.y;
}

// Library method for callback function, that returns nothing
void callback_func(Data& inputStruct)
{
        cout<<"\n callback function";
}
 
// Library method to order subset A and then subset B, and return to the caller.
vector<Data> smallestAscendingY(vector<Data> &subSetA, int n) {
    std::vector<Data> myvector;
    //sort the vector based on value of y
    std::sort(subSetA.begin(), subSetA.end(), cmp_based_on_y);
   
    //pick only n items and return
    //std::copy_n ( subSetA, n, myvector.begin() );
    return std::vector<Data>(subSetA.begin(), subSetA.begin() + n);;
}

// Library method to order subset A and then subset B, and return to the caller.
vector<Data> reOrderTheContainer(vector<Data> &subSetA, vector<Data> &subSetB) {
    std::vector<Data> AB = subSetA;
    AB.insert(AB.end(), subSetB.begin(), subSetB.end());
    return AB;
}

void categorizeTheSet(vector<Data> &container) {
    for(int i = 0; i < inputSize; i++) {
        Data entry = container.at(i);
        if ((unsigned)entry.x > (unsigned)x_threshold) {
            subSetA.push_back(entry);
        } else {
            subSetB.push_back(entry);
        }
    }
}

void printData (vector<Data> &container) {
    for(Data d : container) {
        cout<<"\n x: "<<d.x<<", y: " <<d.y;
    }
}
   

int main()
{
  //input number for assignment 2
  int n;
 
  // The container to hold all the items
  std::vector<Data> container;
 
  std::cout <<"Please enter the x_threshold: ";
  std::cin>>x_threshold;
 
  std::cout <<"Please enter the number of items: ";
  std::cin>>inputSize;
  std::cout << "Entered input size, " << inputSize << "!\n";
 
  // Loop to take the input from the user
  for(int i = 0; i < inputSize; i++) {
    cout<<"\n Enter x and y values, for example : 4, 1.1";
    std::cin>>x;
    std::cin>>y;
    container.push_back({x,y});
  }
 
  // Categorize the set to have subset A and subset B, subset A has the x value greater than threshold.
  categorizeTheSet(container);
 
  cout<<"\nThe container before comparison with threshold: ";
  //Print the container
  printData(container);
 
  cout<<"\nThe container set A: ";
  //Print the setA
  printData(subSetA);
 
  cout<<"\nThe container set B: ";
  //Print the setB
  printData(subSetB);
 
  //Assignment 1: Reorders the container in such a way, that subset A preceeds subset B.
  std::vector<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
 
  cout<<"\nPrint the re-ordered container: ";
  //Print the re-ordered container
  printData(orderedContainer);
 
  //Assignment 2: Reorders elements of subset B in such a way,
  //that given number n of elements has smallest value of y and this n elements are
  //in ascending order of y. Let this n elements be subset C.
  cout<<"\n Enter the number n: ";
  cin>>n;
  std::vector<Data> c = smallestAscendingY(subSetB, n);

  cout<<"\nPrint the c: ";
  //Print the re-ordered container based on y value
  printData(c);
 
  //Declare a callback function that returns void
  void (*ptr)(Data&) = callback_func;
 
  //Call the callback function and pass the subset C
    for(Data dataOfC : c) {
          ptr(dataOfC);
    }
}