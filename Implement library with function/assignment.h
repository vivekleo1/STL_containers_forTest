#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <algorithm>
#include <assert.h>
using namespace std;

struct Data {
    uint8_t x;
    double y;

    Data(uint8_t a, double b) {
        x = a;
        y = b;
    }
};

bool cmp_based_on_y(const Data&, const Data&);
void callback_func(Data&);
std::vector<Data> smallestAscendingY(std::vector<Data>&, int);
vector<Data> reOrderTheContainer(vector<Data>&, vector<Data>&);
void categorizeTheSet(vector<Data>&, uint8_t, vector<Data>&, vector<Data>&);
void printData(vector<Data>&);
