#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <algorithm>
#include <cassert>
#include <assert.h>
#include "assignment.h"
#include <stdio.h>
using namespace std;
#ifdef NDEBUG
#  define assert(condition) ((void)0)
#endif
//Assignment 1: Reorders the container in such a way, that subset A preceeds subset B.
void testReorder()
{
    std::vector<Data> subSetA;
    std::vector<Data> subSetB;
    std::vector<Data> container;
    std::uint8_t x_threshold = 5;
    container.push_back({ 1,1.1 });
    container.push_back({ 3,3.3 });
    container.push_back({ 6,6.7 });
    container.push_back({ 8,8.8 });
    container.push_back({ 9,9.8 });
    categorizeTheSet(container, x_threshold, subSetA, subSetB);
    std::vector<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
     assert(orderedContainer.size() == 5);
    //subSetA size should be 3
    assert(subSetA.size() == 3);
    //subSetB size should be 2
    assert(subSetB.size() == 2);
    //subSetA`s First Element should be {6, 6.7}
    assert(subSetA.at(0).x == 6 && subSetA.at(0).y == 6.7);
 }
//Assignment 2: Reorders elements of subset B in such a way,
//that given number n of elements has smallest value of y and this n elements are
//in ascending order of y. Let this n elements be subset C.

void testAssending()
{
std::vector<Data> subSetA;
std::vector<Data> subSetB;
std::vector<Data> container;
std::uint8_t x_threshold = 5;
container.push_back({ 1,1.1 });
container.push_back({ 3,3.3 });
container.push_back({ 6,6.7 });
container.push_back({ 8,8.8 });
container.push_back({ 9,9.8 });
categorizeTheSet(container, x_threshold, subSetA, subSetB);
std::vector<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
std::vector<Data> c = smallestAscendingY(subSetB, 2);

//First smallestAscending
assert(subSetB.at(0).x == 1 && subSetB.at(0).y == 1.1);
//Second smallestAscending
assert(subSetB.at(1).x == 3 && subSetB.at(1).y == 3.3);
}
//Assignment 3: Call the callback function and pass the subset C
void testCallBack()
{
    ptr callback = callback_func;
    std::vector<Data> container;
    container.push_back({ 1,1.1 });
    Data d = container.at(0);
    callback(d);
 }
void negativeTestReorder()
{
    std::vector<Data> subSetA;
    std::vector<Data> subSetB;
    std::vector<Data> container;
    std::uint8_t x_threshold = 5;
    container.push_back({ 1,1.1 });
    container.push_back({ 3,3.3 });
    container.push_back({ 6,6.7 });
    container.push_back({ 8,8.8 });
    container.push_back({ 9,9.8 });
    categorizeTheSet(container, x_threshold, subSetA, subSetB);
    std::vector<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
    assert(orderedContainer.size() != 5);
    //subSetA size should be 3
    assert(subSetA.size() != 3);
    //subSetB size should be 2
    assert(subSetB.size() != 2);
    //subSetA`s First Element should be {6, 6.7}
    assert(subSetA.at(0).x != 6 && subSetA.at(0).y != 6.7);

}
void negativeTestAssending()
{
    std::vector<Data> subSetA;
    std::vector<Data> subSetB;
    std::vector<Data> container;
    std::uint8_t x_threshold = 5;
    container.push_back({ 1,1.1 });
    container.push_back({ 3,3.3 });
    container.push_back({ 6,6.7 });
    container.push_back({ 8,8.8 });
    container.push_back({ 9,9.8 });
    categorizeTheSet(container, x_threshold, subSetA, subSetB);
    std::vector<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
    std::vector<Data> c = smallestAscendingY(subSetB, 2);

    //First smallestAscending
    assert(subSetB.at(0).x != 1 && subSetB.at(0).y != 1.1);
    //Second smallestAscending
    assert(subSetB.at(1).x != 3 && subSetB.at(1).y != 3.3);
}
int main()
{
    //Test the reorder functionallty 
    testReorder();
    testAssending();
    testCallBack();
    negativeTestReorder();
    negativeTestAssending();
    
}