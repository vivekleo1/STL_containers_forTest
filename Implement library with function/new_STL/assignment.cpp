
#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <algorithm>
#include "assignment.h"
using namespace std;

bool cmp_based_on_y(const Data& a, const Data& b)
{
    // smallest comes first
    return a.y < b.y;
}

// Library method for callback function, that returns nothing
void callback_func(Data& inputStruct)
{
    cout << "\n callback function";
}

// Library method to order subset A and then subset B, and return to the caller.
vector<Data> smallestAscendingY(vector<Data>& subSetA, int n) {
    std::vector<Data> myvector;
    //sort the vector based on value of y
    std::sort(subSetA.begin(), subSetA.end(), cmp_based_on_y);

    //pick only n items and return
    //std::copy_n ( subSetA, n, myvector.begin() );
    return std::vector<Data>(subSetA.begin(), subSetA.begin() + n);;
}

// Library method to order subset A and then subset B, and return to the caller.
vector<Data> reOrderTheContainer(vector<Data>& subSetA, vector<Data>& subSetB) {
    std::vector<Data> AB = subSetA;
    AB.insert(AB.end(), subSetB.begin(), subSetB.end());
    return AB;
}

void categorizeTheSet(vector<Data>& container, uint8_t x_threshold, vector<Data>&subSetA,vector<Data>&subSetB) {
    for (Data entry : container) {
        if ((unsigned)entry.x > (unsigned)x_threshold) {
            subSetA.push_back(entry);
        }
        else {
            subSetB.push_back(entry);
        }
    }
}
void printData(vector<Data>& container) {
    for (Data d : container) {
        cout << "\n x: " << d.x << ", y: " << d.y;
    }
}
// This is the entry point of library
/*Implement library with function which :
1. Reorders the container in such a way, that subset A preceeds subset B.
2. Reorders elements of subset B in such a way, that given number ```n``` of elements has smallest value of y and this n elements are in ascending order of y.Let this ```n``` elements be subset C.
3. Accepts callback as a parameter and applies it to elements of subset C.Callback takes ```Data&``` as parameter and returns no result.*/

void assignmentFunction(vector<Data> &container, uint8_t x_threshold, ptr callback, int n) {
        std::vector<Data> subSetA;
        std::vector<Data> subSetB;
        categorizeTheSet(container, x_threshold, subSetA, subSetB);
        std::vector<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
        std::vector<Data> c = smallestAscendingY(subSetB, n);
        for (Data dataOfC : c) {
            callback(dataOfC);
        }
    }


