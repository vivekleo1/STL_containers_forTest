#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <algorithm>
#include <cassert>
#include <assert.h>
#include "assignment.h"
#include <stdio.h>
using namespace std;
#ifdef NDEBUG
#  define assert(condition) ((void)0)
#endif
//Assignment 1: Reorders the container in such a way, that subset A preceeds subset B.
void TestReorder()
{
    std::vector<Data> subSetA;
    std::vector<Data> subSetB;
    std::vector<Data> container;
    std::uint8_t x_threshold = 5;
    container.push_back({ 1,1.1 });
    container.push_back({ 3,3.3 });
    container.push_back({ 6,6.7 });
    container.push_back({ 8,8.8 });
    container.push_back({ 9,9.8 });
  
    std::vector<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);

    assert("Container size should be 5", orderedContainer.size == 5);
    assert("subSetA size should be 3", subSetA.size == 3);
    assert("subSetB size should be 2", subSetB.size == 2);
    assert("subSetA`s First Element should be {6, 6.7}", subSetA.at(0).x == 6 && subSetA.at(0).y == 6.7);

    std::vector<Data> c = smallestAscendingY(subSetB, 2);
    assert("First smallestAscending", subSetB.at(0).x == 1 && subSetB.at(0).y == 1.1 );
    assert("Second smallestAscending", subSetB.at(1).x == 3 && subSetB.at(1).y == 3.3);

    void (*ptr)(Data&) = callback_func;
   
    assert("callback_func returns no result"); 
    assert("callback_func returns no result");
}
//Assignment 2: Reorders elements of subset B in such a way,
   //that given number n of elements has smallest value of y and this n elements are
   //in ascending order of y. Let this n elements be subset C.

void TestAssending()
{
std::vector<Data> subSetA;
std::vector<Data> subSetB;
std::vector<Data> container;
std::uint8_t x_threshold = 5;
container.push_back({ 1,1.1 });
container.push_back({ 3,3.3 });
container.push_back({ 6,6.7 });
container.push_back({ 8,8.8 });
container.push_back({ 9,9.8 });
std::vector<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);

std::vector<Data> c = smallestAscendingY(subSetB, 2);
assert("First smallestAscending", subSetB.at(0).x == 1 && subSetB.at(0).y == 1.1);
assert("Second smallestAscending", subSetB.at(1).x == 3 && subSetB.at(1).y == 3.3);

}
//Call the callback function and pass the subset C
void TestCallBack()
{
    void (*ptr)(Data&) = callback_func;
    assert("callback_func returns no result");
    assert("callback_func returns no result");
}
int main()
{
    int n;
    uint8_t x_threshold;
    int inputSize;
    uint8_t x;
    double y;
    std::vector<Data> subSetA;
    std::vector<Data> subSetB;
    std::vector<Data> container;

     //Test the reorder functionallty 
    TestReorder();
    TestAssending();
    TestCallBack();
}